# Learning Process

## 1. How to Learn Faster with the Feynman Technique

### What is the Feynman Technique? Paraphrase the video in your own words.
Feynman technique is a 4 step process that one can apply on any kind of topic like a algorithm, first aid technique or business concept to make sure that they understand or learnt it well. The underlying rule is to learn something in such a way that you can explain it to anyone, even a child with zero prior knowledge.
The four steps are -
- Write down the topic name in a sheet of paper
- Explain the topic in your own words and examples if applicable
- Go through source material again if stuck with any part of the explanation
- Go through the sheet of paper and dumb down hard to understand points so even a kid can understand it

### What are the different ways to implement this technique in your learning process?
I can do the following to implement Feynman technique in my learning process
- Creating notes by following Feynman's 4 steps
- Explaining topics / techniques to a rubber duck
- Explain topics to fellow Cohort members

## 2. Learning How to Learn TED talk by Barbara Oakley
### Paraphrase the video in detail in your own words.
Barbara talks about how some of the most brilliant people use the Pomodoro technique which is using one's mind two very common states focused and relaxed mode on command to improve one's learning process.
The Pomodoro technique suggests relaxing one's mind when you are stuck with something. When our mind is relaxed it is free of a lot of cluttered thoughts and at this moment if we can suddenly shift to focus on the stuck part we can overcome it.
She also tells how it's necessary to exercise and test oneself on the topic one is learning.

### What are some of the steps that you can take to improve your learning process?
I can try to implement the Pomodoro technique and test myself frequently on things that I am learning. 

## 3. Learn Anything in 20 hours
### Your key takeaways from the video? Paraphrase your understanding.
Josh talks about how there is a misconception that it takes 10,000 hours to learn any new skill. This 10,000 hours rule was originally about becoming an expert in highly specialized or competitive skills. Josh's own experience and research led him to the conclusion that it takes 20 hours to learn a new skill. 

In his TED talk, he explains that we humans don't want to feel stupid but learning a new skill has an initial stage that makes us feel stupid. This is one of the emotional barriers that we need to overcome and we can learn anything new in about 20 hours.

He suggests following 4 steps to follow while learning something new - 
- Deconstruct the skill into sub parts
- Gather resources and learn enough to self correct
- Remove distractions while learning
- Dedicate 20 hours to learn the thing even if you feel stupid

### What are some of the steps that you can while approaching a new topic?
I can do the following
- Convince myself to not feel stupid
- Remove distractions
- Learn essentials and important parts first, enough to self correct
- Be patient and power through 20 hours 

