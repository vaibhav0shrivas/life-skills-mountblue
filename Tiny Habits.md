# Tiny Habits

### Your takeaways from the video (Minimum 5 points).
- A real change comes through hundreds of small decisions of small habits.
- One can make drastic changes with minor tweaks to the daily routine. It does not require something big.
- One should aim for behavior changes over the outcome.
- One can change your environment to force new habits.
- To develop a habit, have motivation, ability, and a trigger.

### Your takeaways from the video in as much detail as possible.
To form a habit, it's important to start small and gradually build up. One way to do this is to break down the desired behavior into smaller, manageable actions that require little motivation to complete. Additionally, identifying a specific trigger like a regular activity or event can help cultivate the new behavior. Finally, recognizing and celebrating small successes along the way can help boost confidence and motivation to continue making progress.

### How can you use B = MAP to make making new habits easier?
B = MAP essentially says to break the task into small steps, cultivate triggers to make these habits regular, and celebrate your small victories.

### Why it is important to "Shine" or Celebrate after each successful completion of a habit?
It mostly helps to trick your mind into continuing the habit because it excited you.

### Your takeaways from the video (Minimum 5 points)
- Habits need time to develop.
- One should stop and create barriers to bad habits.
- One should make the environment easier for good habits to cultivate.
- Have a proper plan to incorporate habits into your lifestyle.
- Follow the Two-minute rule: If it takes two minutes or less, do it instantly, don't procrastinate it.



### Write about the book's perspective on habit formation from the lens of Identity, processes, and outcomes.

**Identity**
Identity is about believing, how much the habit can be effective.

**Processes**
Processes are about what one does, what one does to achieve the habit or make it regular. There is a focus on the process.

**Outcomes**
Outcomes are about focusing on the outcome of cultivating a habit and aiming for the result so much that the actual habit's cultivation will take a hit.

### Write about the book's perspective on how to make a good habit easier.
A good habit can be cultivated in four stages: 
  - **Cue**: Have triggers to initiate the habit.
  - **Craving**: Increase your craving to do the habit.
  - **Response**: Make the habit easier by creating fewer steps.
  - **Reward**: Celebrate the completion of a habit/task.

### Write about the book's perspective on making a bad habit more difficult.
A habit can be made more difficult by Making it harder to do the habit, removing the satisfaction of the task, and making introspecting the bad effects of the habit.


### Pick one habit that you would like to do more of. What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
I will try to eat healthy, uninstalling delivery apps, and limiting myself to 3-4 meals a day can help me achieve this.

### Pick one habit you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
I will try to stop wasting my weekends by not doing anything and just sleeping in alarms, or making plans on the day before can help.
