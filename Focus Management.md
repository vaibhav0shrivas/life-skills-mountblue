# Focus Management

### What is Deep Work?
Deep Work is a term coined by Newport it means working on something with distractions focus. Naturally, if one is not distracted one can get more and more efficient work done.

### Paraphrase all the ideas in the above videos and [this one](https://www.youtube.com/watch?v=gTaJhjQHcf8) **in detail**.
The first video talked about what's the optimum time for doing deep work, it's quite obvious that you can not have 24hrs of undivided focus or maybe a sage can but for everyone else, it's good practice to go for 1hr or 1.5hrs of deep work where you focus on *The* task and no other out of context things.

The second video talks about why the deadline is good for productivity. Newport says the main reason productivity increases under a deadline or strict deadlines is not because of the deadline itself but because you no longer think about whether you should take a break or not. As that recurring thought is not distracting you from deep work your productivity increases.

The last video tells us that due to the over-connectivity of the modern world, there are far more distractions available or present between you and your work, be it social media or messages in teams everyone wants you to revert immediately. This ability to deep work in today's world is a very valuable skill. Newport's book and the video also talk about how doing deep work over time can help your brain develop kind of better neuron connections that further increases focus and creative ability.

### How can you implement the principles in your day-to-day life?
With time I can implement these principles by doing the following things
- Have a burst of no-distraction works/tasks.
- Have these bursts at the regular time over the weekdays.
- Break from work at the end of the shift/day.

### Your key takeaways from the [video](https://www.youtube.com/watch?v=3E7hkPZ-HTk).
Newport talks about why one should quit social media. He says quitting social media will lead to one respecting his attention span and hence more peace of mind, more productive days, and increased ability to deep work.
He states and provides counter arguement against 3 common concern any social media user has, which are as follows
- It is an essential technology :
    Social media is not an essential technology but an addictive slot machine-like activity.
- It is important to be successful in modern world :
    Social media is not something necessary that you would need to be at the top of your field or area of interest. He says that if your work is good that itself will become your ticket to success.
- It is harmless so why quit it :
    It is in fact not harmless but critically affects your anxiety and attention span.
