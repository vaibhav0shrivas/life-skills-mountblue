# Grit and Growth Mindset

## 1. Grit

#### Paraphrase the [video](https://www.youtube.com/watch?v=H14bBuluwB8) in a few lines. Use your own words.
Angela talks about how the most common thing among successful people in schools, a sales job, or any other company is how _gritty_ they are. Grit just means persevering and putting in the work every day. According to the research that she and her team conducted being gritty is more likely to contribute to one's success in their field even over inherent talent.

#### What are your key takeaways from the video to take action on?
I can understand that the video's main message was to persevere at whatever job I am doing.

## 2. Introduction to Growth Mindset

#### Paraphrase the [video](https://www.youtube.com/watch?v=75GFzikmRY0) in a few lines in your own words.
The video talks about **Growth Mindset** and its counterpart **Fixed Mindset**.
People who have a fixed mindset believe that a person is born with skills and they are in no power of their abilities. Their focus is often on how they look when they try something new and worried about their performance over learning. They see efforts as hectic and unnecessary work. They shy away from challenges. Get too frustrated and bent on their mistakes and don't properly handle feedback.
People who have a growth mindset believe that a person can develop any skill over time and that they are in the power of their abilities. Their focus is mostly on learning the said skill and not so much on how they look during the learning process. They see efforts as means to growth. They welcome challenges, learn from their mistakes and handle feedback by working on improvement.
Lastly, the video talks about how our mind is not a binary value which can either be in fixed or growth mindset, but it is a spectrum and one should actively try and identify what causes them to change their mindset.

#### What are your key takeaways from the video to take action on?
I understood how to identify when I am going into a fixed mindset and though it is very important to keep performance in mind, it is much more important to have a growth mindset during learning anything.

## 3. Understanding Internal Locus of Control
#### What is the Internal Locus of Control? What is the key point in the [video](https://www.youtube.com/watch?v=8ZhoeSaPF-k)?
Internal locus of control basically means having a belief and realizing that the efforts you make and the work you put in are responsible for the outcomes you face/receive. By having an internal locus of control you take charge in your life and are able to be more motivated than others.

## 3. How to build a Growth Mindset
#### Paraphrase the [video](https://www.youtube.com/watch?v=9DVdclX6NzY) in a few lines in your own words.
The video talks about how to build a growth mindset. The presenter talks about how one needs to believe in one self's ability to figure things out on their own. Having this belief helps in working on perseverance. The next thing presenter talks about is how it is important to challenge the assumption you make about your abilities. The importance of having a long-term curriculum to actually grow in life is a very important thing to have and more important to follow. Last but not least he reminds us to honor our hard work we have put in so far when we eventually have a bad day and face failure, honor the failure to be just part of life learn from it and move on.

#### What are your key takeaways from the video to take action on?
These are my key takeaways from the video
- Have belief that you will figure it out.
- Challenge your assumptions.
- Have long-term plans and implement them.
- Accept and honor failure.

## 4. Mindset - [A MountBlue Warrior Reference Manual](https://docs.google.com/document/d/1SPUqC-8WwfiDlsRGKWqoMtC14v6_2TEhq7LZs29bJWk)

#### What are one or more points that you want to take action on from the manual? (Maximum 3)
- I will always be enthusiastic. I will have a smile on my face when I face new challenges or meet people.
- I will understand the users very well. I will serve them and society by writing rock-solid excellent software.
- I am very good friends with Confusion, Discomfort, and Errors.
    - Confusion tells me there is more understanding to be   achieved.
    - Discomfort tells me I have to make an effort to understand. I understand the learning process is uncomfortable.
    - Errors tell me what is not working in my code and how to fix it.
