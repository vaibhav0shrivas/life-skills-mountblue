# Good Practices for Software Development


### What is your one major takeaway from each one of the 6 sections? So 6 points in total.

- **Gathering requirements** - Be clear with requirements and get regular feedback during development as well.
- **Always over-communicate** - Keep all stakeholders informed about when a task can be completed and update on pushed deadlines.
- **Stuck? Ask questions** - Ask direct and clear questions along with screenshots or code snapshots.
- **Get to know your teammates** - Build a good rapport with teammates.
- **Be aware and mindful of other team members** - Do not waste the time of teammates, queries should be made by being mindful of their workload.
- **Doing things with 100% involvement** - Take care of health to have better focus during work.


### Which area do you think you need to improve on? What are your ideas to make progress in that area?

I should work on over-communicating. I tend to overestimate or under-estimate how much time I would need to complete tasks more so when I am stuck and trying to find a solution. I need to communicate about missing deadlines in these scenarios.

I should also work on knowing my teammates because sometimes I keep to myself and come off as unavailable.

