# Listening and Active Communication

## 1. Active Listening

### What are the steps/strategies to do Active Listening? 

One can try to incorporate the following things into their lives to become more of an active listener.

- Remove their distractive thoughts from the mind.
- Focus on the person speaking.
- Avoid interrupting the speaker.
- Respond after the speaker is finished speaking.
- Ask follow-up questions.
- Have interested body language.
- Take notes whenever necessary.
- Paraphrase or repeat to make sure you are on the same page as the speaker.

## 2. Reflective Listening

### According to Fisher's model, what are the key points of Reflective Listening?

Fisher mentions becoming a reflective listener, one has to have a genuine interest in not only the words but also the body language and any other cues of the speaker.

## 3. Reflection

### What are the obstacles in your listening process?

Sometimes topics that are being discussed are not something that I am genuinely interested in. I am not that good at understanding indirect cues be it physical or through a video call, so I might not be on the same page as the speaker.

### What can you do to improve your listening?

I can take notes and after the speaker is finished ask questions and repeat what I understand to make sure I got every thing right.

## 4. Types of Communication

### When do you switch to Passive communication style in your day to day life?
I switch to a passive communication style when I am with unknown people and in a dangerous situation where disagreements can cause me to harm physically or otherwise. Another case would be when I am getting my mistakes pointed out Or I am not sure what to do.

### When do you switch into Aggressive communication styles in your day to day life?
I switch to an aggressive communication style when I am too sure of my opinion to be right and I am trying to convince someone of the same thing.

### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
I switch to a passive-aggressive communication style when I am with friends or familiar people and making off hands remarks or sarcasm would not become a serious issue of disagreement or be seen as outright hateful.


### How can you make your communication assertive? You can analyse the videos  and then think what steps you can apply in your own life? (Watch the videos first before answering this question.)

I can make myself assertive by first understanding what exactly I am feeling. What I need in certain situations. After I have done identifying those two things I need to make sure I keep my point without ignoring the other person's point of view or opinion, but at the same time not get shadowed by it.



