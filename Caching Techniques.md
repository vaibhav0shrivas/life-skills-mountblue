# Caching Techniques

## A Brief About Caching
In computing, a cache is a high-speed data storage layer which stores a subset of data, typically of volatile nature. The cache is a smaller and faster memory that stores copies of the frequently used data. Content like HTML pages, images, files, etc is stored in the cache to improve the efficiency and overall performance of the application. Caching allows you to efficiently reuse previously retrieved or computed data.


## Benefits of Caching

### Improve Application Performance
Because memory is orders of magnitude faster than disk (magnetic or SSD), reading data from in-memory cache is extremely fast (sub-millisecond). This significantly faster data access improves the overall performance of the application.

### Reduce Database Cost
A single cache instance can provide hundreds of thousands of IOPS (Input/output operations per second), potentially replacing a number of database instances, thus driving the total cost down. This is especially significant if the primary database charges per throughput. In those cases the price savings could be dozens of percentage points.

### Reduce the Load on the Backend
By redirecting significant parts of the read load from the backend database to the in-memory layer, caching can reduce the load on your database, and protect it from slower performance under load, or even from crashing at times of spikes.

### Predictable Performance
A common challenge in modern applications is dealing with times of spikes in application usage. Examples include social apps during the Super Bowl or election day, eCommerce websites during Black Friday, etc. Increased load on the database results in higher latencies to get data, making the overall application performance unpredictable. By utilizing a high throughput in-memory cache this issue can be mitigated.

### Eliminate Database
In many applications, it is likely that a small subset of data, such as a celebrity profile or popular product, will be accessed more frequently than the rest. This can result in hot spots in your database and may require overprovisioning of database resources based on the throughput requirements for the most frequently used data. Storing common keys in an in-memory cache mitigates the need to overprovision while providing fast and predictable performance for the most commonly accessed data.

### Increase Read Throughput (IOPS)
In addition to lower latency, in-memory systems also offer much higher request rates (IOPS) relative to a comparable disk-based database. A single instance used as a distributed side-cache can serve hundreds of thousands of requests per second.


## Caching Techniques 

### Database Caching
Data Caching is very important for DB driven applications.
It stores the data in local memory on the server and helps avoid extra trips to the DB for retrieving Data that has not changed. For most DB solutions, cache frequently used queries in order to reduce turnaround time. It is standard practice to clear any cache data after it has been altered. Overuse of Data Caching can cause memory issues if data is constantly added and removed to and from the cache.

### Domain Name System (DNS) Caching
Every domain request made on the internet essentially queries DNS cache servers in order to resolve the IP address associated with the domain name. DNS caching can occur on many levels including on the OS, via ISPs and DNS servers.

### Session Management
HTTP sessions contain the user data exchanged between your site users and your web applications such as login information, shopping cart lists, previously viewed items and so on. Critical to providing great user experiences on your website is managing your HTTP sessions effectively by remembering your user’s preferences and providing rich user context.

### Application Programming Interfaces (APIs)
An API generally is a RESTful web service that can be accessed over HTTP and exposes resources that allow the user to interact with the application. When designing an API, it’s important to consider the expected load on the API, the authorization to it, the effects of version changes on the API consumers and most importantly the API’s ease of use, among other considerations. Sometimes serving a cached result of the API will deliver the most optimal and cost-effective response. This is especially true when you are able to cache the API response to match the rate of change of the underlying data.


## References 
- [Caching Techniques: One should know.](https://bootcamp.uxdesign.cc/caching-techniques-one-should-know-603e09d2b298)
- [Caching Overview - AWS.](https://aws.amazon.com/caching/)
- [Caching Patterns - AWS.](https://docs.aws.amazon.com/whitepapers/latest/database-caching-strategies-using-redis/caching-patterns.html)
- [Caching Strategies and How to Choose the Right One - CODEAHOY.](https://codeahoy.com/2017/08/11/caching-strategies-and-how-to-choose-the-right-one/)
