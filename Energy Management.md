# Energy Management

### What are the activities you do that make you relax - Calm quadrant?
- Cleaning my room
- Listening to music
- Playing certain games

### When do you find getting into the Stress quadrant?
- Closing in on a deadline
- Thinking about the future

### How do you understand if you are in the Excitement quadrant?
- I get more energetic
- I get happier

### Paraphrase the Sleep is your Superpower video in detail.
The speaker talks about the importance of sleep, he opens up with the fact that people who are not sleeping well for a long time are physically aging faster. He talks about an experiment showing how sleep-deprived people are comparatively bad at learning compared to the control group with good sleep. He talks about studies proving why a regular good amount of sleep is a necessity to avoid health issues like increased chance of heart attacks, poor immunity, and stress.

### What are some ideas that you can implement to sleep better?
I personally think I get a good amount of sleep on a regular basis, I have a fixed time for dinner, do-not-disturb on my phone, and an alarm to wake up in the mornings.

### Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.
Following are some of the brain-changing benefits of Exercise.
- Improved focus
- Promotes growth of brain cells
- Protects your brain from aging
- Decreases anxiety
- Generates happy hormones

### What are some steps you can take to exercise more?
I can work on making working out part of my lifestyle/routine and get into a sport that is fun and can be part of my life for a long.
