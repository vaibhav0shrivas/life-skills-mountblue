# Prevention of Sexual Harassment


## What kinds of behavior cause sexual harassment?
There are mainly three forms of sexual harassment.
- **Verbal sexual harassment** - Sexual jokes, comments on body or clothing
- **Visual sexual harassment** - Inappropriate posters, gifs, screensavers, text messages
- **Physical sexual harassment** - Sexual assault, blocking someone's path, inappropriate touching

Any kind of behavior that is of sexual nature or has underlying sexual innuendo can be a cause of sexual harassment. The aforementioned behaviors all come under sexual harassment.


## What would you do in case you face or witness any incident or repeated incidents of such behavior?

In any case, where I face sexual harassment or witness someone else facing the same, I will inform the workplace safety officer or the HR manager with a documented email of what happened or what I witnessed.

